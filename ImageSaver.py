#!/usr/bin/env python3
import depthai as dai
import cv2
import time

# Parameters, change these and not the code
RESOLUTION = [720, 480] # Resolution of the images saved
FreqDelay = 30 # Delay between images in seconds
MaxImg = 4000 # Max images taken for each individual camera, total images is this value times two
TimeOfDayActiveHours = [5, 23] # Active hours of the day for collecting images, first is lower bound, second is upper bound, will only save images in between
OfflineWaitTime = 60 # Delay when outside of active hours before checking again

# Create pipeline
pipeline = dai.Pipeline()

# Define sources and output
cam = pipeline.create(dai.node.ColorCamera)
cam.setBoardSocket(dai.CameraBoardSocket.RGB)
cam.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
cam.setVideoSize(RESOLUTION[0], RESOLUTION[1])

xout = pipeline.create(dai.node.XLinkOut)
xout.setStreamName("outStream")
xout.input.setBlocking(False)
xout.input.setQueueSize(1)

# Linking
cam.video.link(xout.input)

# Device1 setup
device1Info = dai.DeviceInfo("10.0.0.122")
device1 = dai.Device(pipeline, device1Info)
qOut1 = device1.getOutputQueue(name="outStream", maxSize=1, blocking=False)

# Device2 setup
device2Info = dai.DeviceInfo("10.0.0.123")
device2 = dai.Device(pipeline, device2Info)
qOut2 = device2.getOutputQueue(name="outStream", maxSize=1, blocking=False)

# Process loop
imagesTaken = 0
while True:
    currentHour = int(time.strftime("%H"))
    print("Current hour", currentHour)
    if ((currentHour < TimeOfDayActiveHours[0]) or (currentHour > TimeOfDayActiveHours[1])):
        time.sleep(OfflineWaitTime)
        continue

    frame1 = qOut1.get().getCvFrame()
    res1 = cv2.imwrite("/home/pi/image-saver/SavedImages/Cam1/" + str(time.time()) + ".png", frame1)
    print("res1:", res1)

    frame2 = qOut2.get().getCvFrame()
    res2 = cv2.imwrite("/home/pi/image-saver/SavedImages/Cam2/" + str(time.time()) + ".png", frame2)
    print("res2:", res2)
    
    imagesTaken = imagesTaken + 1
    if imagesTaken >= MaxImg:
        break

    time.sleep(FreqDelay)
    
