echo "Creating dirs"
mkdir SavedImages
cd SavedImages
mkdir Cam1
mkdir Cam2
cd ..

echo "Setting up service"
cp image-saver.service /etc/systemd/system/
systemctl enable image-saver
systemctl daemon-reload
systemctl start image-saver